window.$ = window.jQuery;

const $window = $(window);

$window.on('load', function () {

    const section = $('.timeline section');

    setTimeout(() => {
        $(section[0]).addClass('visible');
    }, 1000)

    setTimeout(() => {
        $(section[1]).addClass('visible');
    }, 1200)

    $window.on('scroll', () => {
        section.each(function () {
            const thisSection = $(this);
            const thisSectionTop = thisSection.offset().top;
            const thisSectionBot = thisSectionTop + thisSection.height();
            const screenTop = $window.scrollTop();
            const screenBot = $window.scrollTop() + $window.innerHeight();

            if (screenBot > thisSectionTop && screenTop < thisSectionBot) {
                $(thisSection).addClass('visible');
            }
        })
    })

});